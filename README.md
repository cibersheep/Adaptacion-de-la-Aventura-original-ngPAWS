# Adaptación de la Aventura original a ngPAWS
Recreación de la Aventura original de Aventuras AD (versión reducida y en castellano de _Advent._

## Registro de cambios
- Inicial. Test para aprender el lenguaje **ngPAWS**
- Versión final. Unidas las dos partes originales de la aventura. Mejora en reconocimiento de verbos. Usados gráficos de la versión de Amiga. Añadidos gráficos de las localidades que no tenían. 
- Arreglados errores. No se podía examinar cierto objeto, el hablar con ciertos personajes no jugadores era difícil y faltaba algún sinónimo (gracias @baltasarq)

## Aplicación
- App para teléfonos con Ubuntu Touch [descargable aquí](https://open-store.io/app/aventuraoriginal.cibersheep)
- Versión online en [gitlab] (https://cibersheep.gitlab.io/Adaptacion-de-la-Aventura-original-ngPAWS/)

### Versión archivada antigua
- Versión online en github https://cibersheep.github.io/Adaptacion-de-la-Aventura-original-ngPAWS/